package com.alzan.testwirecard

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.alzan.testwirecard.api.Api
import com.alzan.testwirecard.api.ApiCallback
import com.alzan.testwirecard.orders.OrdersActivity
import org.json.JSONException
import org.json.JSONObject


class LoginActivity : AppCompatActivity() {

    val api = Api()

    lateinit var sharedPref: SharedPreferences

    lateinit var loginButton: Button
    lateinit var passwordField: EditText
    lateinit var emailField: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        emailField = findViewById(R.id.emailField)
        passwordField = findViewById(R.id.passwordField)
        loginButton = findViewById(R.id.loginButton)

        sharedPref = getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)

        loginButton.setOnClickListener { v ->
            doLogin(emailField.text.toString(), passwordField.text.toString())
        }
    }

    fun doLogin(email: String, password: String) {
        val callback = object : ApiCallback {
            override fun onSuccess(response: String) {
                try {
                    val jsonObject = JSONObject(response)
                    val token = jsonObject.getString("accessToken")

                    if (!token.isEmpty() || !token.isBlank() || !token.equals("")) {
                        setUserSession(token)
                        startActivity(Intent(applicationContext, OrdersActivity::class.java))
                    } else {
                        showAlert()
                    }

                } catch (ex: JSONException) {
                    ex.printStackTrace()
                    showAlert()
                }
            }

            override fun onError(error: Any) {
            }
        }

        api.auth(
            "https://connect-sandbox.moip.com.br/oauth/token",
            email, password, callback
        )
    }

    fun showAlert() {
        runOnUiThread {
            Toast.makeText(
                applicationContext,
                "Os dados informados estão incorretos. Tente novamente!",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun setUserSession(token: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.clear()
        editor.putString("token", token)
        editor.apply()
    }
}

