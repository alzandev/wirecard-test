package com.alzan.testwirecard

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.alzan.testwirecard.orders.OrdersActivity

class StartActivity : AppCompatActivity() {

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        sharedPreferences = getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)

        try {
            val token: String = sharedPreferences.getString("token", null)

            if (token == null) {
                startActivity(Intent(this, LoginActivity::class.java))
            } else {
                startActivity(Intent(this, OrdersActivity::class.java))
            }
        } catch (ex: Exception) {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}
