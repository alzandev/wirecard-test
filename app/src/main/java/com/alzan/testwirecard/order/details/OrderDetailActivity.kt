package com.alzan.testwirecard.order.details

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.alzan.testwirecard.R
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import android.widget.ScrollView
import android.widget.TextView
import com.alzan.testwirecard.api.Api
import com.alzan.testwirecard.api.ApiCallback
import com.alzan.testwirecard.model.detail.Model
import com.alzan.testwirecard.model.orders.Orders
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import org.json.JSONObject
import java.text.SimpleDateFormat


class OrderDetailActivity : AppCompatActivity() {

    lateinit var value: TextView
    lateinit var ownId: TextView

    lateinit var payMethod: TextView
    lateinit var orderIdLabel: TextView

    lateinit var customerName: TextView
    lateinit var customerEmail: TextView

    lateinit var createdAt: TextView

    lateinit var statusDate: TextView
    lateinit var status: TextView

    lateinit var totalAmount: TextView
    lateinit var fees: TextView
    lateinit var liquid: TextView

    lateinit var installmentCount: TextView

    lateinit var progressBar: ProgressBar
    lateinit var scrollView: ScrollView


    var orderId: String? = ""
    val api = Api()
    lateinit var sharedPref: SharedPreferences
    var gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_detail)

        val intent = intent
        val orderId = intent.getStringExtra("orderId")

        value = findViewById(R.id.value)
        ownId = findViewById(R.id.ownId)

        payMethod = findViewById(R.id.payMethod)
        orderIdLabel = findViewById(R.id.orderId)

        customerName = findViewById(R.id.customer)
        customerEmail = findViewById(R.id.customerEmail)

        createdAt = findViewById(R.id.createdAt)

        statusDate = findViewById(R.id.statusDate)
        status = findViewById(R.id.status)

        totalAmount = findViewById(R.id.totalAmount)
        fees = findViewById(R.id.fees)
        liquid = findViewById(R.id.liquid)

        installmentCount = findViewById(R.id.installmentCount)

        progressBar = findViewById(R.id.progressBar)
        scrollView = findViewById(R.id.scrollView)

        sharedPref = getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)

        title = "Pedido: $orderId"
        this.orderId = orderId

        getData()
    }

    fun getData() {
        val callback = object : ApiCallback {

            override fun onSuccess(response: String) {
                parseJson(response)
            }

            override fun onError(error: Any) {
            }
        }

        api.get(
            "https://sandbox.moip.com.br/v2/orders/$orderId",
            sharedPref.getString("token", null), callback
        )
    }

    fun parseJson(data: String) {
        val typeToken = object : TypeToken<JsonObject>() {}.type
        val type = object : TypeToken<Model>() {}.type

        var jsonObject: JsonObject = gson.fromJson(data, typeToken)

        val model: Model = gson.fromJson(jsonObject, type)

        var currency = "$"

        when (model.amount.currency) {
            "BRL" -> {
                currency = "R$"
            }
            "USD" -> {
                currency = "US$"
            }
        }

        value.text = currency + String.format("%.2f", (model.amount.total.toFloat() / 100))
        orderIdLabel.text = model.id
        ownId.text = model.ownId

        when (model.payments[0].fundingInstrument.method) {
            "CREDIT_CARD" -> payMethod.text = "Cartão de crédito"
            "BOLETO" -> payMethod.text = "Boleto"
        }

        customerName.text = model.customer.fullname
        customerEmail.text = model.customer.email

        createdAt.text = formatDate(model.createdAt)
        statusDate.text = formatDate(model.updatedAt)

        when (model.status) {
            "PAID" -> {
                status.text = "PAGO"
                status.setTextColor(Color.parseColor("#FF2E8F32"))
            }
            "REVERTED" -> {
                status.text = "REVERTIDO"
                status.setTextColor(Color.parseColor("#3295C7"))
            }
            "WAITING" -> {
                status.text = "AGUARDANDO"
                status.setTextColor(Color.parseColor("#FFEB3B"))
            }
            "NOT_PAID" -> {
                status.text = "NÃO PAGO"
                status.setTextColor(Color.parseColor("#FF0000"))
            }
        }

        totalAmount.text = "+ " + currency + String.format("%.2f", (model.amount.total.toFloat() / 100))
        fees.text = "- " + currency + String.format("%.2f", (model.amount.fees.toFloat() / 100))
        liquid.text = "= " + currency + String.format("%.2f", (model.amount.liquid.toFloat() / 100))

        if (model.payments[0].installmentCount > 1) {
            installmentCount.text = "${model.payments[0].installmentCount.toString()} pagamentos"
        } else {
            installmentCount.text = "${model.payments[0].installmentCount.toString()} pagamento"
        }

        scrollView.visibility = VISIBLE
        progressBar.visibility = GONE
    }

    fun formatDate(date: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        val date = sdf.parse(date)
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")

        return dateFormat.format(date).toString()
    }
}
