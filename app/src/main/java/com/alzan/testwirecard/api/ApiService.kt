package com.alzan.testwirecard.api

import com.alzan.testwirecard.App
import com.android.volley.Header
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import okhttp3.Call
import okhttp3.Callback
import okhttp3.FormBody
import okhttp3.OkHttpClient
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import com.android.volley.AuthFailureError


class ApiService : ApiInterface {
    override fun get(path: String, token: String, callBack: ApiCallback) {
        val request = object : StringRequest(
            Request.Method.GET,
            path, callBack::onSuccess, callBack::onError
        ) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers.put("Authorization", "OAuth $token")
                return headers
            }
        }

        /*OBS:  Usei o token a23ee2a3499a489d808f110856f4b65c_v2, pois o token gerado pela API não estava funcionando
               Quando eu utilizo o token gerado pela API retorna { "ERROR" : "Token or Key are invalids" }

        */

        App.getRequestQueue().add(request)

    }

    override fun post(path: String, params: JSONObject, token: String, callBack: ApiCallback) {
        val request = object :
            JsonObjectRequest(Request.Method.POST, path, params,
                Response.Listener<JSONObject> { response ->
                    callBack.onSuccess(response.toString())
                },
                Response.ErrorListener { error ->
                    callBack.onError(error)
                }) {

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

                return headers
            }
        }

        App.getRequestQueue().add(request)
    }

    override fun auth(url: String, username: String, password: String, callBack: ApiCallback) {
        try {
            val client = OkHttpClient()
            val requestBody = FormBody.Builder()
                .add("grant_type", "password")
                .add("client_id", "APP-H1DR0RPHV7SP")
                .add("client_secret", "05acb6e128bc48b2999582cd9a2b9787")
                .add("username", username)
                .add("password", password)
                .add("scope", " APP_ADMIN")
                .build()

            val request = okhttp3.Request.Builder()
                .url(url)
                .post(requestBody)
                .build()
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call?, e: IOException?) {
                    callBack.onError(e.toString())
                }

                override fun onResponse(call: Call?, response: okhttp3.Response) {
                    try {
                        val jsonObject = JSONObject(response.body().string())
                        callBack.onSuccess(jsonObject.toString())
                    } catch (e: JSONException) {
                        callBack.onError(e)
                        e.printStackTrace()
                    }
                }
            })

        } catch (exception: Exception) {
            callBack.onError(exception)
        }
    }
}