package com.alzan.testwirecard.api

import com.google.gson.JsonObject
import org.json.JSONObject

interface ApiInterface {
    fun get(path: String, token: String, callBack: ApiCallback)
    fun post(path: String, params: JSONObject, token: String, callBack: ApiCallback)
    fun auth(url: String, username: String, password: String, callBack: ApiCallback)
}