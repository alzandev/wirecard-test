package com.alzan.testwirecard.api

import com.android.volley.VolleyError

interface ApiCallback {
    fun onSuccess(response: String)
    fun onError(error: Any)
}

