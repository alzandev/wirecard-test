package com.alzan.testwirecard.api

import org.json.JSONObject

class Api : ApiInterface{
    private var service = ApiService()

    override fun get(path: String, token: String, callBack: ApiCallback) {
        service.get(path, token, callBack)
    }

    override fun post(path: String, params: JSONObject, token: String, callBack: ApiCallback) {
        service.post(path, params, token, callBack)
    }

    override fun auth(url: String, username: String, password: String, callBack: ApiCallback) {
        service.auth(url, username, password, callBack)
    }
}