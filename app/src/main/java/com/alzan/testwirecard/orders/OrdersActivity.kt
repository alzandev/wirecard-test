package com.alzan.testwirecard.orders

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.alzan.testwirecard.R
import com.alzan.testwirecard.api.Api
import com.alzan.testwirecard.api.ApiCallback
import com.alzan.testwirecard.model.orders.Orders
import com.alzan.testwirecard.model.orders.Summary
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import org.json.JSONObject


class OrdersActivity : AppCompatActivity() {

    val api = Api()

    lateinit var recyclerView: RecyclerView
    lateinit var adapter: RecyclerAdapter
    lateinit var sharedPref: SharedPreferences
    lateinit var totalAmount: TextView

    lateinit var mainLayout: LinearLayout
    lateinit var progressBar: ProgressBar

    lateinit var errorMessage: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_orders)

        title = "Pedidos"

        recyclerView = findViewById(R.id.recyclerView)
        totalAmount = findViewById(R.id.amount)
        mainLayout = findViewById(R.id.mainLayout)
        progressBar = findViewById(R.id.progressBar)
        errorMessage = findViewById(R.id.errorMessage)

        val layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)

        recyclerView.layoutManager = layoutManager

        sharedPref = getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)

        getData()

    }

    fun getData() {
        val callback = object : ApiCallback {
            override fun onSuccess(response: String) {
                parseJson(response)
            }

            override fun onError(error: Any) {
                progressBar.visibility = GONE
                errorMessage.visibility = VISIBLE
            }
        }

        api.get(
            "https://sandbox.moip.com.br/v2/orders",
            sharedPref.getString("token", null), callback
        )
    }

    var gson = Gson()
    var ordersArray = ArrayList<Orders>()
    lateinit var orders: Orders


    fun parseJson(data: String) {

        val typeToken = object : TypeToken<ArrayList<JsonObject>>() {}.type
        val type = object : TypeToken<Orders>() {}.type

        val jsonObject = JSONObject(data)
        val ordersJson = jsonObject.getString("orders")

        var jsonArray: List<JsonObject> = gson.fromJson(ordersJson, typeToken)

        for (item in jsonArray) {
            val ordersItem: Orders = gson.fromJson(item, type)
            ordersArray.add(ordersItem)
        }

        val summaryType = object : TypeToken<Summary>() {}.type
        val summaryTypeToken = object : TypeToken<JsonObject>() {}.type
        val summaryJson = jsonObject.getString("summary")

        var summaryJsonObject: JsonObject = gson.fromJson(summaryJson, summaryTypeToken)
        val summary: Summary = gson.fromJson(summaryJsonObject, summaryType)

        totalAmount.text =
            "${summary.count} pedidos totalizando R$ ${String.format("%.2f", (summary.amount.toFloat() / 100))}"
        adapter = RecyclerAdapter()
        adapter.orders(applicationContext, ordersArray)
        recyclerView.adapter = adapter

        mainLayout.visibility = VISIBLE
        progressBar.visibility = GONE
        errorMessage.visibility = GONE


    }

    override fun onBackPressed() {
    }
}
