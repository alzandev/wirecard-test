package com.alzan.testwirecard.orders

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alzan.testwirecard.R
import com.alzan.testwirecard.model.orders.Orders
import com.alzan.testwirecard.order.details.OrderDetailActivity
import java.text.SimpleDateFormat
import java.util.*


class RecyclerAdapter : RecyclerView.Adapter<RecyclerHolder>() {

    lateinit var orders: ArrayList<Orders>

    lateinit var item: Orders

    lateinit var context: Context

    fun orders(context: Context, orders: ArrayList<Orders>) {
        this.orders = orders
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.order_item, null)
        return RecyclerHolder(view)
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onBindViewHolder(holder: RecyclerHolder, position: Int) {

        item = orders[position]

        var currency = "$"

        when (item.amount.currency) {
            "BRL" -> {
                currency = "R$"
            }
            "USD" -> {
                currency = "US$"
            }
        }

        holder.value.text = currency + String.format("%.2f", (item.amount.total.toFloat() / 100))

        holder.email.text = item.customer.email
        holder.id.text = item.ownId

        if (item.updatedAt != null) {
            holder.date.text = formatDate(item.updatedAt)
        } else {
            holder.date.text = formatDate(item.createdAt)
        }

        when (item.payments.first().fundingInstrument.method) {
            "CREDIT_CARD" -> holder.paymentMethod.setImageResource(R.drawable.ic_credit_card)
            "BOLETO" -> holder.paymentMethod.setImageResource(R.drawable.ic_product_barcode)
        }


        when (item.status) {
            "PAID" -> {
                holder.status.text = "PAGO"
                holder.status.setTextColor(Color.parseColor("#FF2E8F32"))
            }
            "REVERTED" -> {
                holder.status.text = "REVERTIDO"
                holder.status.setTextColor(Color.parseColor("#3295C7"))
            }
            "WAITING" -> {
                holder.status.text = "AGUARDANDO"
                holder.status.setTextColor(Color.parseColor("#FFEB3B"))
            }
            "NOT_PAID" -> {
                holder.status.text = "NÃO PAGO"
                holder.status.setTextColor(Color.parseColor("#FF0000"))
            }
        }


        holder.cardView.setOnClickListener { v ->
            val intent = Intent(context, OrderDetailActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("orderId", orders[position].id)

            context.startActivity(intent)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun formatDate(date: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss-SSS")
        val date = sdf.parse(date)
        val dateFormat = SimpleDateFormat("dd/MM/yyyy")

        return dateFormat.format(date).toString()
    }
}