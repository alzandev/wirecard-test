package com.alzan.testwirecard.orders

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.alzan.testwirecard.R


class RecyclerHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var value: TextView = itemView.findViewById(R.id.value)
    var date: TextView = itemView.findViewById(R.id.date)
    var email: TextView = itemView.findViewById(R.id.email)
    var status: TextView = itemView.findViewById(R.id.status)
    var id: TextView = itemView.findViewById(R.id.id)
    var paymentMethod: ImageView = itemView.findViewById(R.id.paymentMethod)
    var cardView: CardView = itemView.findViewById(R.id.cardView)

}