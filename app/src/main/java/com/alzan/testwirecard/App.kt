package com.alzan.testwirecard

import android.app.Application
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class App : Application() {
    companion object {
        private lateinit var instance: App

        fun getInstance() = instance
        fun getRequestQueue(): RequestQueue = Volley.newRequestQueue(getInstance().applicationContext)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}