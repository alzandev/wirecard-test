package com.alzan.testwirecard.model.detail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Payments(

    @SerializedName("id") val id: String,
    @SerializedName("status") val status: String,
    @SerializedName("delayCapture") val delayCapture: Boolean,
    @SerializedName("amount") val amount: Amount,
    @SerializedName("installmentCount") val installmentCount: Int,
    @SerializedName("fundingInstrument") val fundingInstrument: FundingInstrument,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("updatedAt") val updatedAt: String
) : Parcelable