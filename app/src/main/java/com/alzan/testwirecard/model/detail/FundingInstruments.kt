package com.alzan.testwirecard.model.detail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FundingInstruments(
    @SerializedName("method") val method: String
) : Parcelable