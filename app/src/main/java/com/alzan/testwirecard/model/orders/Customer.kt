package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Customer(
    @SerializedName("fullname") val fullname: String,
    @SerializedName("email") val email: String
) : Parcelable