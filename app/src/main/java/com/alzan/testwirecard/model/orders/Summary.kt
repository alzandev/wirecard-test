package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Summary(
    @SerializedName("count") val count: Int,
    @SerializedName("amount") val amount: Int
) : Parcelable