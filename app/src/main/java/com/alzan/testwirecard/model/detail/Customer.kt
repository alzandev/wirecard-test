package com.alzan.testwirecard.model.detail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Customer(

    @SerializedName("id") val id: String,
    @SerializedName("ownId") val ownId: Int,
    @SerializedName("fullname") val fullname: String,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("birthDate") val birthDate: String,
    @SerializedName("email") val email: String,
    @SerializedName("fundingInstruments") val fundingInstruments: List<FundingInstruments>
) : Parcelable