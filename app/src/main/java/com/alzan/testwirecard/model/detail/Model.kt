package com.alzan.testwirecard.model.detail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Model(

    @SerializedName("id") val id: String,
    @SerializedName("ownId") val ownId: String,
    @SerializedName("status") val status: String,
    @SerializedName("platform") val platform: String,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("updatedAt") val updatedAt: String,
    @SerializedName("amount") val amount: Amount,
    @SerializedName("customer") val customer: Customer,
    @SerializedName("payments") val payments: List<Payments>,
    @SerializedName("escrows") val escrows: List<String>,
    @SerializedName("refunds") val refunds: List<String>
) : Parcelable