package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Payments(
    @SerializedName("id") val id: String,
    @SerializedName("installmentCount") val installmentCount: Int,
    @SerializedName("fundingInstrument") val fundingInstrument: FundingInstrument
) : Parcelable