package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Orders(
    @SerializedName("id") val id: String,
    @SerializedName("ownId") val ownId: String,
    @SerializedName("status") val status: String,
    @SerializedName("blocked") val blocked: Boolean,
    @SerializedName("amount") val amount: Amount,
    @SerializedName("customer") val customer: Customer,
    @SerializedName("items") val items: List<Items>,
    @SerializedName("payments") val payments: List<Payments>,
    @SerializedName("createdAt") val createdAt: String,
    @SerializedName("updatedAt") val updatedAt: String
) : Parcelable