package com.alzan.testwirecard.model.detail

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Amount(
    @SerializedName("total") val total: Int,
    @SerializedName("currency") val currency: String,
    @SerializedName("fees") val fees: Int,
    @SerializedName("refunds") val refunds: Int,
    @SerializedName("liquid") val liquid: Int

) : Parcelable