package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FundingInstrument(
    @SerializedName("method") val method: String,
    @SerializedName("brand") val brand: String
) : Parcelable