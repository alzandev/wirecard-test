package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Items(
    @SerializedName("product") val product: String
) : Parcelable