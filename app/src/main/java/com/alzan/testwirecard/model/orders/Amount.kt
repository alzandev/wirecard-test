package com.alzan.testwirecard.model.orders

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Amount(
    @SerializedName("total") val total: Int,
    @SerializedName("addition") val addition: Int,
    @SerializedName("fees") val fees: Int,
    @SerializedName("deduction") val deduction: Int,
    @SerializedName("otherReceivers") val otherReceivers: Int,
    @SerializedName("currency") val currency: String
) : Parcelable